//
//  CMTime + Extention.swift
//  MyMusic
//
//  Created by Михаил on 04.12.2019.
//  Copyright © 2019 Михаил Колотилин. All rights reserved.
//

import Foundation
import AVKit

extension CMTime {
    
    // функция замены CMTime на String в формате мин:сек
    func toDisplayString() -> String {
        // проверка на отсутствие данных
        guard !CMTimeGetSeconds(self).isNaN else { return "" }
        let totalSeconds = Int(CMTimeGetSeconds(self))
        // выделение количества секунд
        let seconds = totalSeconds % 60
        // выделение количества минут
        let minutes = totalSeconds / 60
        // форматирование в нужный формат
        let timeFormatString = String(format: "%02d:%02d", minutes,seconds)
        return timeFormatString
    }
    
}
