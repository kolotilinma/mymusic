//
//  NetworkService.swift
//  MyMusic
//
//  Created by Михаил on 02.12.2019.
//  Copyright © 2019 Михаил Колотилин. All rights reserved.
//

import UIKit
import Alamofire

class NetworkService {
    func fetchTracks(searchText: String, completion: @escaping (SearchResponse?) -> Void) {
        let url = "https://itunes.apple.com/search"
        let parameters = ["term":"\(searchText)",
                          "limit":"25",
                          "media":"music"]
        
        // Создание запроса Alamofire для получения данных по url с параметрами parameters
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            // блок обработки ошибки ответа данных dataResponse
            if let error = dataResponse.error {
                print("Error received requestiong data: \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = dataResponse.data else { return }
            // блок обработки декодирование JSON
            let decoder = JSONDecoder()
            do {
                // попытка декодирования и запись в objects
                let objects = try decoder.decode(SearchResponse.self, from: data)
               completion(objects)
            } catch let jsonError {
                // блок если ошибка декодирования
                print("Failed to decode JSON", jsonError)
                completion(nil)
            }
        }
    }
}

