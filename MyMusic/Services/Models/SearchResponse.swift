//
//  SearchResponse.swift
//  MyMusic
//
//  Created by Михаил on 02.12.2019.
//  Copyright © 2019 Михаил Колотилин. All rights reserved.
//

import Foundation

struct SearchResponse: Decodable {
    var resultCount: Int
    var results: [Track]
}

struct Track: Decodable {
    var trackName: String
    var collectionName: String?
    var artistName: String
    var artworkUrl100: String?
    var previewUrl: String?
}
