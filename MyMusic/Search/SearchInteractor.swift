//
//  SearchInteractor.swift
//  MyMusic
//
//  Created by Михаил on 02.12.2019.
//  Copyright (c) 2019 Михаил Колотилин. All rights reserved.
//

import UIKit

protocol SearchBusinessLogic {
    func makeRequest(request: Search.Model.Request.RequestType)
}

class SearchInteractor: SearchBusinessLogic {
    
    var networkService = NetworkService()
    var presenter: SearchPresentationLogic?
    var service: SearchService?
    
    func makeRequest(request: Search.Model.Request.RequestType) {
        if service == nil {
            service = SearchService()
        }
        switch request {
            
        case .getTracks(let searchText):
//            print("Interactor .getTracks")
            presenter?.presentData(response: .presentFooterView)
            networkService.fetchTracks(searchText: searchText) { [weak self] (searchResponse) in
                self?.presenter?.presentData(response: Search.Model.Response.ResponseType.presentTracks(searchResponse: searchResponse))
            }
        }
    }
}
