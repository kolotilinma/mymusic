//
//  TrackDetailView.swift
//  MyMusic
//
//  Created by Михаил on 04.12.2019.
//  Copyright © 2019 Михаил Колотилин. All rights reserved.
//

import UIKit
import SDWebImage
import AVKit

protocol TrackMovingDelegate: class {
    // описание функций перехода по треку
    func moveBackForPreviosTrack() -> SearchViewModel.Cell?
    func moveForwardForPreviosTrack() -> SearchViewModel.Cell?
}

class TrackDetailView: UIView {
    
    @IBOutlet weak var viewTrackImageView: UIView!
    @IBOutlet weak var trackImageView: UIImageView!
    @IBOutlet weak var currentTimeSlider: UISlider!
    @IBOutlet weak var correntTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var trackTitleLabel: UILabel!
    @IBOutlet weak var authorTitleLabel: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var volumeSlider: UISlider!
    
    // создание плеера на странице
    let player: AVPlayer = {
        let avPlayer = AVPlayer()
        // параметр для максимально быстрого старта воспроизведения игнорируя возможность остановки при не полной буферизации
        avPlayer.automaticallyWaitsToMinimizeStalling = false
        return avPlayer
    }()
    
    // создание делегата
    weak var delegate: TrackMovingDelegate?
    
    // MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        setupImageView()
    }
    
    // MARK: - Setup
    // настройка trackImageView и теней
    func setupImageView() {
        // уменьшение по умолчанию trackImageView
        let scale: CGFloat = 0.8
        viewTrackImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
        let imageCornerRadius: CGFloat = 10
        // скругление картинки
        trackImageView.layer.cornerRadius = imageCornerRadius
        // настройка теней
        viewTrackImageView.clipsToBounds = false
        viewTrackImageView.layer.shadowColor = UIColor.darkGray.cgColor
        viewTrackImageView.layer.shadowOpacity = 1
        viewTrackImageView.backgroundColor = .none
        viewTrackImageView.layer.shadowOffset = CGSize.zero
        viewTrackImageView.layer.shadowRadius = imageCornerRadius
        viewTrackImageView.layer.shadowPath = UIBezierPath(roundedRect: viewTrackImageView.bounds, cornerRadius: 10).cgPath
    }
    
    //функция назначения параметров с переданными данными
    func set(viewModel: SearchViewModel.Cell) {
        // назначаем track и author Title из переданных данных
        trackTitleLabel.text = viewModel.trackName
        authorTitleLabel.text = viewModel.artistName
        
        // попытка включить трек по ссылке
        playTrack(previewUrl: viewModel.previewUrl)
        // отслеживание начала воспроизведения и выполнение анимации
        monitorStartTime()
        // вызов функции для обозревания и записи проигранных секунд песни
        observePlayerCorrentTime()
        // подменяем 100x100 в запросе на 600x600
        let string600 = viewModel.iconUrlString?.replacingOccurrences(of: "100x100", with: "600x600")
        
        // создаем тип URL из string600
        guard let url = URL(string: string600 ?? "") else { return }
        
        // назначаем картинку при помощи SDWebImage по url
        trackImageView.sd_setImage(with: url, completed: nil)
    }
    
    private func playTrack(previewUrl: String?) {
        // создаем тип URL previewUrl
        guard let url = URL(string: previewUrl ?? "") else { return }
        // создаем элемент для проигрования по URL
        let playerItem = AVPlayerItem(url: url)
        // добавляем элемент в плеер
        player.replaceCurrentItem(with: playerItem)
        // запускаем проигрывание
        player.play()
    }
    
    // MARK: - Time setup
    private func monitorStartTime() {
        
        let time = CMTimeMake(value: 1, timescale: 3)
        let times = [NSValue(time: time)]
        // параметр для отслеживания момента старта воспроизведения
        player.addBoundaryTimeObserver(forTimes: times, queue: .main) { [weak self] in
            self?.enlargeTrackImageView()
        }
    }
    // обозреватель изменения количества проигранных секунд песни
    private func observePlayerCorrentTime() {
        // создание периода обозревания равное 1 сек
        let interval = CMTimeMake(value: 1, timescale: 2)
        // выбираем элемент для обозревания
        player.addPeriodicTimeObserver(forInterval: interval, queue: .main) { [weak self] (time) in
            // запись начала отсчета в correntTimeLabel
            self?.correntTimeLabel.text = time.toDisplayString()
            // создание общего количество времени
            let durationTime = self?.player.currentItem?.duration
            // создание разницы времени с начала обозревания
            let currentDurationText = ((durationTime ?? CMTimeMake(value: 1, timescale: 1)) - time).toDisplayString()
            // запись данных в durationLabel
            self?.durationLabel.text = "-\(currentDurationText)"
            self?.updateCurrentTimeSlider()
        }
    }
    
    private func updateCurrentTimeSlider() {
        let currentTimeSeconds = CMTimeGetSeconds(player.currentTime())
        let durationSeconds = CMTimeGetSeconds(player.currentItem?.duration ?? CMTimeMake(value: 1, timescale: 1))
        let percentage = currentTimeSeconds / durationSeconds
        self.currentTimeSlider.value = Float(percentage)
    }
    
    deinit {
        print("TrackDetailView Удален из памяти")
    }
    
    // MARK: - Animations
    // Анимация trackImageView при начале воспроизведении
    private func enlargeTrackImageView() {
        // withDuration: "время воспроизведения", delay: "задержка", usingSpringWithDamping: "ускорение", initialSpringVelocity: "ускорение", options: "направление анимации", animations: "что анимируем", completion: "что по завершении")
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.viewTrackImageView.transform = .identity
        }, completion: nil)
    }
    
    private func reduceTrackImageView() {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            let scale: CGFloat = 0.8
            self.viewTrackImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: nil)
    }
    
    
    // MARK: - IBAction
    @IBAction func handleCurrentTimeSlider(_ sender: Any) {
        // создание елемента введеных даннных %
        let percentage = currentTimeSlider.value
        // проверка наличия элемента в проигрывателе
        guard let duration = player.currentItem?.duration else { return }
        // создание элемента времени из текущего трека
        let durationInSeconds = CMTimeGetSeconds(duration)
        // вычисление процента введеных данных
        let seekTimeInSeconds = Float64(percentage) * durationInSeconds
        // создание нужного формата периодом 1 сек
        let seekTime = CMTimeMakeWithSeconds(seekTimeInSeconds, preferredTimescale: 1)
        // установка значения в плеер
        player.seek(to: seekTime)
    }
    
    @IBAction func handleVolumeSlider(_ sender: Any) {
        player.volume = volumeSlider.value
    }
    
    @IBAction func dragDownButtonTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    @IBAction func previousTrack(_ sender: Any) {
        let cellViewModel = delegate?.moveBackForPreviosTrack()
        guard let cellInfo = cellViewModel else { return }
        self.set(viewModel: cellInfo)
    }
    
    @IBAction func nextTrack(_ sender: Any) {
        let cellViewModel = delegate?.moveForwardForPreviosTrack()
        guard let cellInfo = cellViewModel else { return }
        self.set(viewModel: cellInfo)
    }
    
    @IBAction func playPauseAction(_ sender: Any) {
        if player.timeControlStatus == .paused {
            player.play()
            playPauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            enlargeTrackImageView()
        } else {
            player.pause()
            playPauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            reduceTrackImageView()
        }
    }
}
