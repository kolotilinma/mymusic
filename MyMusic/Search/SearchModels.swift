//
//  SearchModels.swift
//  MyMusic
//
//  Created by Михаил on 02.12.2019.
//  Copyright (c) 2019 Михаил Колотилин. All rights reserved.
//

import UIKit

enum Search {
    
    enum Model {
        struct Request {
            enum RequestType {
                case getTracks(searchText: String)
            }
        }
        struct Response {
            enum ResponseType {
                case presentTracks(searchResponse: SearchResponse?)
                case presentFooterView
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayTracks(searchViewModel: SearchViewModel)
                case displayFooterView
            }
        }
    }
}

struct SearchViewModel {
    struct Cell: TrackCellModel {
        var iconUrlString: String?
        var trackName: String
        var collectionName: String?
        var artistName: String
        var previewUrl: String?
    }
    let cells: [Cell]
}
